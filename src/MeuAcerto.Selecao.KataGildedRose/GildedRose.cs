﻿using System.Collections.Generic;

namespace MeuAcerto.Selecao.KataGildedRose
{
    class GildedRose
    {
        IList<Item> Itens;
        public GildedRose(IList<Item> Itens)
        {
            this.Itens = Itens;
        }

        public void AtualizarQualidade()
        {
            for (int i = 0; i < Itens.Count ; i++)
            { //varre a lsita

                if (Itens[i].Nome != "Queijo Brie Envelhecido" && Itens[i].Nome!="Ingressos para o concerto do Turisas" && Itens[i].Nome!="Dente do Terrasque")
                { //nao faz isso nos itens que envelhecem bem.

                    if (Itens[i].PrazoParaVenda <= 0)
                    {
                        Itens[i].Qualidade -= 2;
                    }
                    else
                    {
                        Itens[i].Qualidade--;
                    }

                }

                if (Itens[i].Nome == "Bolo de Mana Conjurado")
                {
                    Itens[i].Qualidade--;
                }


                //testa os itens que envelhece
                if (Itens[i].Nome == "Queijo Brie Envelhecido")
                {
                   if(Itens[i].PrazoParaVenda <= 0)
                    {
                        Itens[i].Qualidade += 2;
                    }
                    else
                    {
                        Itens[i].Qualidade++;
                    }

                 }

                if (Itens[i].Nome == "Ingressos para o concerto do Turisas")
                {
                    if (Itens[i].PrazoParaVenda <= 0)
                    {
                        Itens[i].Qualidade = 0;
                    }
                    else
                    {
                        if (Itens[i].PrazoParaVenda <= 10)
                        {
                            if (Itens[i].PrazoParaVenda <= 5)
                            {
                                Itens[i].Qualidade += 3;
                            }
                            else
                            {
                                Itens[i].Qualidade += 2;
                            }
                        }
                        else
                        {
                            Itens[i].Qualidade++;
                        }
                    }

                   

                }


                // Dente do terrasque não altera a qualidade ou prazo
                if (Itens[i].Nome == "Dente do Tarrasque")
                {
                    Itens[i].Qualidade = 80;
                }

                //diminui o prazo de venda
                if (Itens[i].Nome != "Dente do Tarrasque")
                {

                    Itens[i].PrazoParaVenda = Itens[i].PrazoParaVenda - 1;
                }

                
                //faz o teto interior da qualidade
                if (Itens[i].Qualidade < 0)
                {
                    Itens[i].Qualidade = 0;
                }

                //faz o teto superior da qualidade
                if (Itens[i].Qualidade > 50 && Itens[i].Nome != "Dente do Tarrasque")
                {
                    Itens[i].Qualidade = 50;
                }
            }
            
        }
    }
}
